# README #

How to run automated motifbreakR

### What is this repository for? ###

* The [git repo](https://bitbucket.org/dennishazelett/motifbreakr)
* The [Docker image](https://dockerhub.com/dennishazelett/motifbreakr)
* The [motifbreakR manuscript](http://www.ncbi.nlm.nih.gov/pubmed/26272984)

### How do I get set up? ###

Create a directory with a list of snps as a simple text file, 1 per line, then run command:

    docker run -v <full local path to directory with text file>:/home/rstudio/data dennishazelett/motifbreakr


motifbreakr will create a file called 'motifbreakr_result.csv' in your local directory.

### How do I process my VCF file? ###

As before, set up an empty directory with a vcf file. The directory must be **COMPLETELY** empty apart from the vcf file. Then run:

    docker run -v <full local path to directory with vcf file>:/home/rstudio/data dennishazelett/motifbreakr